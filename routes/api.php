<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:sanctum')->group(function() {
    Route::get('timeline', 'Api\Timeline\TimelineController@index')->name('timeline');
    Route::post('tweets', 'Api\Tweets\TweetsController@store')->name('tweets.store');
    Route::post('tweets/{tweet}/likes', 'Api\Tweets\TweetLikeController@store')->name('tweets.like.store');
    Route::delete('tweets/{tweet}/likes', 'Api\Tweets\TweetLikeController@destroy')->name('tweets.like.destroy');
});

