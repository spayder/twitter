<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tweets()
    {
        return $this->hasMany(Tweet::class);
    }

    public function following(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'followers', 'user_id', 'following_id'
        );
    }

    public function followers(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'followers', 'following_id', 'user_id'
        );
    }

    public function tweetsFromFollowers(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough(
            Tweet::class,
            Follower::class,
            'user_id',
            'user_id',
            'id',
            'following_id'
        );
    }

    public function avatar()
    {
        // pravatar just for development
        return 'https://i.pravatar.cc/150?u=' . $this->email;

        // gravatar
//        return 'https://gravatar.com/avatar/' . md5($this->email) . '?d=mp';
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function hasLiked(Tweet $tweet)
    {
        return $this->likes->where('tweet_id', $tweet->id)->count();
    }
}
