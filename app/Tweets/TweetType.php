<?php

namespace App\Tweets;

class TweetType
{
    CONST TWEET = 'tweet';
    CONST RETWEET = 'retweet';
    CONST QUOTE = 'quote';
}
