<?php

namespace App\Http\Controllers\Api\Tweets;

use App\Events\Tweets\TweetWasLiked;
use App\Http\Controllers\Controller;
use App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TweetLikeController extends Controller
{
    public function store(Tweet $tweet, Request $request)
    {
        if ($request->user()->hasLiked($tweet)) {
            return response(null, Response::HTTP_CONFLICT);
        }

        $request->user()->likes()->create([
            'tweet_id' => $tweet->id
        ]);

        broadcast(new TweetWasLiked($request->user(), $tweet));
    }

    public function destroy(Tweet $tweet, Request $request)
    {
        $request->user()->likes->where('tweet_id', $tweet->id)->first()->delete();

        broadcast(new TweetWasLiked($request->user(), $tweet));
    }
}
