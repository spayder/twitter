<?php

namespace App\Http\Controllers\Api\Tweets;

use App\Events\Tweets\TweetWasCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tweets\TweetStoreRequest;
use App\Tweets\TweetType;

class TweetsController extends Controller
{
    public function store(TweetStoreRequest $request)
    {
        $data = array_merge($request->only(['body']), [
            'type' => TweetType::TWEET
        ]);

        $tweet = auth()->user()->tweets()->create($data);

        broadcast(new TweetWasCreated($tweet));
    }
}
