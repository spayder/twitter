<?php

namespace App\Http\Controllers\Api\Timeline;

use App\Http\Controllers\Controller;
use App\Http\Resources\TweetCollection;
use Illuminate\Http\Request;

class TimelineController extends Controller
{
    public function index()
    {
        $tweets = auth()->user()
            ->tweetsFromFollowers()
            ->latest()
            ->with(['user', 'likes'])
            ->paginate(5);

        return new TweetCollection($tweets);
    }
}
